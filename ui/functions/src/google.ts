import * as func from 'firebase-functions'
const fetch = require('node-fetch')

export class Google {
  private apiKey: string | undefined
  private calId: string | undefined
  private url: string

  constructor () {
    const googleConfig = func.config().google
    this.calId = googleConfig.calid
    this.apiKey = googleConfig.apikey
    this.url = `https://www.googleapis.com/calendar/v3/calendars/${this.calId}/events`

  }

  async getEvents(month: any, year: any) {
    const min = new Date(year, month - 1)
    const max = new Date(year, month, 0)

    console.log(min)

    const params = {
      key: this.apiKey,
      timeMin: min.toISOString(),
      timeMax: max.toISOString(),
    }

    let response =
        await fetch(`${this.url}?orderBy=startTime&singleEvents=true&timeMin=${params.timeMin}&timeMax=${params.timeMax}&key=${params.key}`)
    response = await response.json()

    const events = []

    for (let i = 0; i < response.items.length; i++)
     events[i] = this.convertToCalendarEvent(response.items[i])

    return events
  }

  convertToCalendarEvent(eventData: any): CalendarEvent {
    const event: CalendarEvent = {
      title: eventData.summary,
      description: eventData.description,
      attachments: eventData.attachments,
      start: eventData.start.dateTime,
      end: eventData.end.dateTime,
    }

    return event
  }

}
