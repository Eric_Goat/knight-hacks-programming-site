import {Google} from './google'
import * as cors from 'cors'
import {https} from 'firebase-functions'
import {Request} from 'firebase-functions/lib/providers/https'

const mw = cors({ origin: true })
export const getEvents = https.onRequest((req, res) => mw(req, res, () => getEventsFunction(req, res)))

export const getEventsFunction = async (req: Request, res: any) => {
    const google = new Google()


    try {
        console.log('Month')
        console.log(req.query.month)
        console.log('Year')
        console.log(req.query.year)
        const events = await google.getEvents(req.query['month'], req.query['year'])
        // res.set('Access-Control-Allow-Credentials', 'true')
        // res.set('Access-Control-Allow-Origin': 'https://knighthacksdev.z19.web.core.windows.net',)
        res.status(200).send({
            data: events
        })
        // res.send = {
        //     status: 200,
        //     headers: {
        //
        //
        //         'Access-Control-Allow-Methods': 'GET'
        //     },
        //     body: {
        //         data: events
        //     }
        // }
    } catch (err) {
        console.log(err)
        res.status(500).send({ err: err })
        // res.send = {
        //     status: 500,
        //     headers: {
        //         'Access-Control-Allow-Credentials': true,
        //         'Access-Control-Allow-Origin': 'https://knighthacksdev.z19.web.core.windows.net',
        //         'Access-Control-Allow-Methods': 'GET'
        //     },
        //     body: {
        //         err: err
        //     }
        // }
    }
}
