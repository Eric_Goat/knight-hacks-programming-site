interface CalendarEvent {
  title: string
  description: string
  attachments: Attachment[]
  start: string
  end: string
}

interface Attachment {
  id: string
  url: string
  filename: string
  type: string
}
