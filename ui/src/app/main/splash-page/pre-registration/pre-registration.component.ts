import { Component, OnInit , Input} from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { EmailSignupComponent} from '../../email-signup/email-signup.component'
import { GenerativeArt } from '../../../generative-art/generative-art'
import { Router } from '@angular/router'



@Component({
  selector: 'app-pre-registration',
  templateUrl: './pre-registration.component.html',
  styleUrls: ['./pre-registration.component.scss']
})
export class PreRegistrationComponent implements OnInit {
  generativeArt: GenerativeArt
  @Input() backButton: boolean
  constructor(private router: Router) { }

  ngOnInit() {
    
    this.generativeArt = new GenerativeArt('canvas')

  }

  goBack() {
    this.router.navigate([''])
  }

}
