import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { GenerativeArt } from '../../generative-art/generative-art'

@Component({
  selector: 'splash-page',
  templateUrl: './splash-page.component.html',
  styleUrls: ['./splash-page.component.scss']
})
export class SplashPageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  
  }

  btnClick = function () {
    this.router.navigateByUrl('/pre-registration')
  }

}
