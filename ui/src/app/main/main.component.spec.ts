import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { MainComponent } from './main.component'
import { SplashPageComponent } from './splash-page/splash-page.component'
import { WorkshopsListComponent } from './workshops-list/workshops-list.component'
import { EmailSignupComponent } from './email-signup/email-signup.component'
import { CalendarDateFormatter, CalendarModule, CalendarUtils, DateAdapter } from 'angular-calendar'
import { MatDialog, MatInputModule } from '@angular/material'
import { SocialLinksComponent } from './email-signup/social-links/social-links.component'

describe('MainComponent', () => {
  let component: MainComponent
  let fixture: ComponentFixture<MainComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MainComponent,
        SplashPageComponent,
        WorkshopsListComponent,
        EmailSignupComponent,
        SocialLinksComponent,
      ],
      imports: [
        CalendarModule,
        MatInputModule,
      ],
      providers: [
        CalendarUtils,
        DateAdapter,
        CalendarDateFormatter,
      ]
    })
      .compileComponents()
  }))
  //
  // beforeEach(() => {
  //   fixture = TestBed.createComponent(MainComponent)
  //   component = fixture.componentInstance
  //   fixture.detectChanges()
  // })
  //
  // it('should create', () => {
  //   expect(component).toBeTruthy()
  // })
})
