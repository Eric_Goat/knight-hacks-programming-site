import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDescriptionMobileComponent } from './event-description-mobile.component';

describe('EventDescriptionMobileComponent', () => {
  let component: EventDescriptionMobileComponent;
  let fixture: ComponentFixture<EventDescriptionMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDescriptionMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDescriptionMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
