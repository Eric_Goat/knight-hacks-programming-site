import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core'
import { CalendarEvent } from '../../../models/calendar-event'
import { HttpService } from '../../../services/http.service'
import * as moment from 'moment'
import { Moment } from 'moment'


@Component({
  selector: 'calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent implements OnInit {
  // @ViewChild('calendar', {static: false}) calendar
  @ViewChild('calendar') calendar
  @Input() events: CalendarEvent[]
  @Input() selectedDay: Moment
  @Output() changeDay = new EventEmitter<Moment>()
  @Output() changeMonth = new EventEmitter<String>()

  daysArr: Moment[]

  constructor(private http: HttpService) {
  }

  ngOnInit() {
    this.daysArr = this.createCalendar(this.selectedDay)
  }

  ngAfterViewInit() {
    this.resizeCalendar()
  }

  changeSelectedDay(day: Moment) {
    if (!this.differentMonth(day)) {
      this.changeDay.emit(day)
      return
    }

    let direction = day.month()-this.selectedDay.month()

    // Checks for month in new year
    if (Math.abs(direction) > 1)
      direction = -Math.sign(direction)

    if (direction > 0)
      this.changeSelectedMonth('next')
    else
      this.changeSelectedMonth('previous')

    this.changeDay.emit(day)
  }

  changeSelectedMonth(direction: String) {
    this.changeMonth.emit(direction)

    this.daysArr = this.createCalendar(this.selectedDay)
  }

  resizeCalendar() {
    const cal = this.calendar.nativeElement
    const container = cal.parentElement

    if (container.clientHeight <= container.clientWidth) {
      container.style.flexDirection = 'column'
      cal.style.width = `${cal.clientHeight}px`
      cal.style.height = '0'
    } else {
      container.style.flexDirection = 'row'
      cal.style.height = `${cal.clientWidth}px`
      cal.style.width = '0'
    }

    let textSizePercent, textSize

    // Resize header text
    textSizePercent = 0.3
    const header : HTMLElement = document.querySelector('#header')
    textSize = header.clientHeight * textSizePercent
    header.style.fontSize = `${textSize}px`

    // Resize calendar text
    textSizePercent = 0.05
    const calElement : HTMLElement = document.querySelector('#calendar')
    textSize = calElement.clientWidth * textSizePercent
    calElement.style.fontSize = `${textSize}px`
  }

  // Highlight the currrent day
  isSelected(day) {
    if (!day) return false

    return (this.selectedDay.format('L') === day.format('L'))
  }

  todayCheck(day) {
    if (!day)
      return false;

    const today = moment()

    return today.format('L') === day.format('L');
    
  }

  // fade out Days that are not in the current month
  differentMonth(day) {
    if (!day) return false

    // checks if the months are different
    return !moment(this.selectedDay).isSame(day, 'M')
  }

  createCalendar(month: Moment): Moment[] {
    const firstDay = moment(month).startOf('M')
    const startOfMonthPadding = firstDay.weekday()
    const endOfMonthPadding = 42 - (startOfMonthPadding + (month.daysInMonth()))

    // Adding the days of the current month to the calendar
    const days = Array.apply(null, {length: month.daysInMonth()})
      .map(Number.call, Number)
      .map((n) => {
        return (moment(firstDay)).add(n, 'd')
      })

    // Adding the days for the previous month at the beggening of the calendar
    for (let n = 0; n < startOfMonthPadding; n++) {
      days.unshift(moment(month).subtract(1, 'M').endOf('M').subtract(n, 'd'))
    }

    // Adding the days for the following month at the end of the calendar
    for (let n = 0; n < endOfMonthPadding; n++) {
      days.push(moment(month).add(1, 'M').startOf('M').add(n, 'd'))
    }

    return days
  }

  numEventsOnDay(day : Moment) {
    if (!day || this.differentMonth(day) || !this.events)
      return 0

    let count = 0
    this.events.forEach(event => {
      if (moment(event.start).isSame(day, 'day'))
        count++
    })

    return count
  }

  isBadgeVisible(day : Moment) {
    return this.numEventsOnDay(day) > 0
  }
}
