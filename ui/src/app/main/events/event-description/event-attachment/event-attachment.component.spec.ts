import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventattachmentComponent } from './event-attachment.component';

describe('EventAttachmentComponent', () => {
  let component: EventattachmentComponent;
  let fixture: ComponentFixture<EventAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
