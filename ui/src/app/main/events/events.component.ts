import { Component, OnInit } from '@angular/core'
import { CalendarEvent } from '../../models/calendar-event'
import { HttpService } from '../../services/http.service'
import * as moment from 'moment'
import { Moment } from 'moment'

@Component({
  selector: 'event-view',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  events: CalendarEvent[] = []
  eventBuffer: CalendarEvent[][] = []

  selectedDay: Moment = moment().startOf('day')
  selectedEvent: CalendarEvent = {
    title: '',
    description: '',
    attachments: [],
    start: null,
    end: null
  }

  constructor(private http: HttpService) {}

  ngOnInit() {
    this.refreshEvents()
  }

  refreshEvents() {
    const currentDay = this.selectedDay

    // Get current month
    let method = `getEvents?month=${this.selectedDay.month() + 1}&year=${this.selectedDay.year()}`
    console.log(method)
    this.http.get(method)
      .subscribe(resp  => {
        this.events = resp.data
        this.selectedEvent = this.getEventByDate(this.selectedDay)
      })

    // Get previous month
    currentDay.subtract(1, 'month')
    method = `GetEvents?month=${currentDay.month() + 1}&year=${currentDay.year()}`
    this.http.get(method)
      .subscribe(resp  => {
        this.eventBuffer[0] = resp.data
      })

    // Get next month
    currentDay.add(2, 'month')
    method = `GetEvents?month=${currentDay.month() + 1}&year=${currentDay.year()}`
    this.http.get(method)
      .subscribe(resp  => {
        this.eventBuffer[1] = resp.data
      })

    // Reset selected day to normal
    currentDay.subtract(1, 'month')
  }

  setSelectedEvent(event: CalendarEvent) {
    this.selectedEvent = event
  }

  setSelectedDay(day: Moment) {
    this.selectedDay = day
    this.setSelectedEvent(this.getEventByDate(day))
  }

  getEventByDate(date: Moment) {
    // Check for months other than date
    if (date.month() < this.selectedDay.month())
      return this.events[this.events.length - 1]
    else if (date.month() > this.selectedDay.month())
      return this.events[0]

    if (!this.events)
      return {
        title: '',
        description: '',
        attachments: [],
        start: null,
        end: null
      }

    // Gets next event relative to now
    for (const event of this.events) {
      const eventDate = moment(event.start)

      if (eventDate.isAfter(date))
        return event
    }

    // Always returns closest event to date
    return this.events[this.events.length - 1]
  }

  setSelectedMonth(direction: String) {
    if (direction === 'next') {
      this.events = this.eventBuffer[0]
      this.selectedDay.add(1, 'month')
    } else if (direction === 'previous') {
      this.events = this.eventBuffer[1]
      this.selectedDay.subtract(1, 'month')
    }

    this.refreshEvents()
  }
}
