import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { EmailSignupComponent } from './email-signup.component'
import { FormsModule } from '@angular/forms'
import { SocialLinksComponent } from './social-links/social-links.component'
import { MatInputModule } from '@angular/material'
import { NoopAnimationsModule } from '@angular/platform-browser/animations'

describe('EmailSignupComponent', () => {
  let component: EmailSignupComponent
  let fixture: ComponentFixture<EmailSignupComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EmailSignupComponent,
        SocialLinksComponent
      ],
      imports: [
        MatInputModule,
        NoopAnimationsModule
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailSignupComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
