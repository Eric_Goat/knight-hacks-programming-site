export interface CalendarEvent {
  title: string,
  description: string,
  attachments: Attachment[]
  start: Date
  end: Date
}

export interface Attachment {
  fileId: string
  fileUrl: string
  iconLink: string
  title: string
}
