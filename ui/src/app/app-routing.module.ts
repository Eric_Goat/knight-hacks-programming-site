import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { MainComponent } from './main/main.component'
import { EventDescriptionMobileComponent } from './main/events/event-description-mobile/event-description-mobile.component'
import { EventsComponent } from './main/events/events.component'
import { MatBadgeModule } from '@angular/material'
import { PreRegistrationComponent } from './main/splash-page/pre-registration/pre-registration.component'
import { AppComponent } from './app.component'
import { componentFactoryName } from '@angular/compiler'

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        component: EventsComponent
      },
      {
        path: 'event-description',
        component: EventDescriptionMobileComponent
      },
    ],
  },

  {
    path: 'pre-registration',
    component: PreRegistrationComponent,
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule, MatBadgeModule]
})
export class AppRoutingModule { }
