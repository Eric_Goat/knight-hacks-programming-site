export const sigmoid = (x) => {
  return 1 / (1 + Math.exp(-10 * (x - 0.2)))
}
